﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;
using UnityEngine.Playables;


public class PlayerController : MonoBehaviour {
    // Start is called before the first frame update
    [Header ("General")]
    [Tooltip ("In ms^-1")][SerializeField] float xSpeed = 4f;
    [Tooltip ("In ms^-1")][SerializeField] float ySpeed = 4f;
    [Tooltip ("In m")][SerializeField] float xRange = 8f;
    [Tooltip ("In m")][SerializeField] float yRange = 4f;
    [SerializeField]PlayableDirector shipTimeline = null;

    [SerializeField] ParticleSystem[] guns;
      
    [Header ("Screen-Position Base")]
    [SerializeField] float positionPitchFactor = -5f;
    [SerializeField] float controlPitchFactor = -10f;

    [Header ("Control-Throw Base")]
    [SerializeField] float positionYawFactor = 5f;
    [SerializeField] float controlRollFactor = -20f;

    [Header ("FX")]
    [Tooltip("FX prefab on player")][SerializeField] GameObject accelerateFX = null;
    float xThrow, yThrow;
    bool alive = true;
    void Start () {
        QualitySettings.vSyncCount = 1;
        Application.targetFrameRate = 60;
        deactivateGuns();
    }
    // Update is called once per frame
    void Update () {
        if (alive) {
            processTranslation ();
            processRotation ();
            processAccelerate ();
            processFiring();
        }
    }
    private void processAccelerate () {
        if (CrossPlatformInputManager.GetButtonDown ("Jump")) {
            accelerate(shipTimeline);
            accelerateFX.SetActive(true);
        }
        if (CrossPlatformInputManager.GetButtonUp ("Jump")){
            deaccelerate(shipTimeline);
            accelerateFX.SetActive(false);
        }
    }
    private void processTranslation () {
        xThrow = CrossPlatformInputManager.GetAxis ("Horizontal");
        yThrow = CrossPlatformInputManager.GetAxis ("Vertical");

        float xOffset = xThrow * xSpeed * Time.deltaTime;
        float yOffset = yThrow * ySpeed * Time.deltaTime;

        float newXPos = Mathf.Clamp (transform.localPosition.x + xOffset, -xRange, xRange);
        float newYPos = Mathf.Clamp (transform.localPosition.y + yOffset, -yRange, yRange);

        transform.localPosition = new Vector3 (newXPos, newYPos, transform.localPosition.z);
    }
    private void processRotation () {
        float pitchDueToPosition = transform.localPosition.y * positionPitchFactor;
        float pitchDueToControl = yThrow * controlPitchFactor;
        float pitch = pitchDueToPosition + pitchDueToControl;

        float yaw = transform.localPosition.x * positionYawFactor;
        float roll = xThrow * controlRollFactor;

        transform.localRotation = Quaternion.Euler (pitch, yaw, roll);
    }
    void onPlayerDeath () { //called by string reference
        alive = !alive;
    }
    void processFiring(){
        if (CrossPlatformInputManager.GetButtonDown("Fire1")){
            activateGuns();
        }
        else if(CrossPlatformInputManager.GetButtonUp("Fire1")){
            deactivateGuns();
        }
    }
    void activateGuns(){
        for(int i = 0; i<guns.Length; i++){
            guns[i].Play();
        }
    }
    void deactivateGuns(){
        for(int i = 0; i < guns.Length; i++){
            guns[i].Stop();
        }
    }
    private void accelerate(PlayableDirector playableDirector){
        playableDirector.playableGraph.GetRootPlayable(0).SetSpeed(2f);
        playableDirector.Play();
    }
    private void deaccelerate(PlayableDirector playableDirector){
        playableDirector.playableGraph.GetRootPlayable(0).SetSpeed(1f);
        playableDirector.Play();
    }
}