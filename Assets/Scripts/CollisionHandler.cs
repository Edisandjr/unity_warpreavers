﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CollisionHandler : MonoBehaviour {
    // Start is called before the first frame update
    [SerializeField] int healthPoints = 100;
    [Tooltip("In seconds")][SerializeField] float levelLoadDelay = 3f;
    [SerializeField] float playerImmunityTime = 3f;
    [Tooltip("FX prefab on player")][SerializeField] GameObject deathFX = null;
    Collider playerCollider;

    void OnCollisionEnter (Collision collision) {
        healthPoints = 0;
        deathSequence();
    }
    void OnTriggerEnter (Collider other) {
        healthPoints = 0;
        deathSequence();
    }
    void Start () { 
        playerCollider = GetComponent<Collider>();
    }

    // Update is called once per frame
    void Update () {

    }
    private void reset () {
        int currentSceneIndex = SceneManager.GetActiveScene ().buildIndex;
        SceneManager.LoadScene (currentSceneIndex);
    }
    public void deathSequence(){
        SendMessage ("onPlayerDeath");
        deathFX.SetActive(true);
        Invoke("reset", levelLoadDelay);
    }
    private void OnParticleCollision (GameObject other) {
        print("particle collision!");
        processDamage(100);
    }
    private void processDamage(int damage){
        healthPoints = healthPoints - damage;
        if (healthPoints <= 0){
            deathSequence();
        }
        toggleCollider();
        Invoke("toggleCollider", playerImmunityTime);
    }
    private void toggleCollider(){
        playerCollider.enabled = !playerCollider;
    }
}