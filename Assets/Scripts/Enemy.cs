﻿using UnityEngine;

public class Enemy : MonoBehaviour {
    // Start is called before the first frame update
    [SerializeField] GameObject deathFX;
    [SerializeField] Transform parent;
    [SerializeField] int scorePerHit = 12;
    [SerializeField] int healthPoints = 100;
    [SerializeField] int scoreOnKill = 100;

    [SerializeField] float hitBoxX = 1f;
    [SerializeField] float hitBoxY = 1f;
    [SerializeField] float hitBoxZ = 1f;

    
    [SerializeField] float offsetX = 0f;
    [SerializeField] float offsetY = 0f;
    [SerializeField] float offsetZ = 0f;
    ScoreBoard scoreBoard;
    CollisionHandler collisionHandler;
    void Start () {
        offsetCollider(changeColliderSize (AddNonTriggerBoxCollider ()));

        scoreBoard = FindObjectOfType<ScoreBoard> ();
    }

    // Update is called once per frame
    void Update () {

    }

    void OnParticleCollision (GameObject other) {
        healthPoints = healthPoints - 10; // add different values based on current guns
        scoreBoard.scoreHit (scorePerHit);
        if (healthPoints <= 0) {
            //consider hit fx/sound
            killEnemy ();
        }
    }

    BoxCollider AddNonTriggerBoxCollider () {
        BoxCollider boxCollider = gameObject.AddComponent<BoxCollider> ();
        boxCollider.isTrigger = false;
        return boxCollider;
    }
    BoxCollider changeColliderSize (BoxCollider boxCollider) {
        boxCollider.size = new Vector3 (hitBoxX, hitBoxY, hitBoxZ);
        return boxCollider;
    }
    BoxCollider offsetCollider (BoxCollider boxCollider){
        boxCollider.center = new Vector3 (offsetX, offsetY, offsetZ);
        return boxCollider;
    }
    void killEnemy () {
        GameObject fx = Instantiate (deathFX, transform.position, Quaternion.identity);
        fx.transform.parent = parent;
        Destroy (gameObject);
        scoreBoard.scoreHit (scoreOnKill);
    }

}