﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreBoard : MonoBehaviour {
    // Start is called before the first frame update

    int score;
    Text scoreText;

    float timer = 0.0f;

    void Start () {
        scoreText = GetComponent<Text>();
        scoreText.text = score.ToString();
    }

    // Update is called once per frame
    public void scoreHit(int scorePerHit){
        score = score + scorePerHit;
        scoreText.text = score.ToString();
    }
    public void scoreTime(){ //funciton bugged.
        timer += Time.deltaTime;
        score = (int)(timer % 60f);
        scoreText.text = score.ToString();
    }
}